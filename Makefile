.PHONY: generate
generate:
	@rm -rf go
	@mkdir go
	@docker run -v $(PWD):/defs namely/protoc-all:1.37_2 -i schemas -f service.proto -o go -l go
	@docker run --rm -v $(PWD)/:/out -v $(PWD)/schemas:/protos hub.chainstorage.io:3003/helios/protoc-gen-doc --doc_opt=markdown,README.md